# UA - Ukraine
JSON schema version: 1.3.0  
Used for productive DCCs issuance  
  
From: 29.07.2021  
Until:  
## Test files for regular cases  
### Vaccination  
2 timely doses of the vaccine COMIRNATY® by BioNTech Manufacturing GmbH  
  
### Recovery  
The diagnosis of COVID-19 was confirmed on July 1, 2021 by PCR test. The recovery COVID-certificate was issued on July 19, 2021 and is valid until December 27, 2021  
  
### Test  
  
  
## Special cases and deviations  
Vaccination cert issued by UA where vaccination country is not UA  
VAC  
  
Recovery cert issued by UA where recovery country is not UA  
REC  
